console.log('app module')

angular.module('app' ,['ngRoute', 'ngMessages'])

angular.module('app')
  .config(($routeProvider) => {
    console.log('route config')
    $routeProvider
      .when('/', {
        templateUrl: '../../partials/login.html',
      })
      .when('/questions', {
        templateUrl: '../../partials/questions.html'
      })
      .when('/newAnswer/:id', {
        templateUrl: '../../partials/new_answer.html'
      })
     .when('/newQuestion', {
       templateUrl: '../../partials/new_question.html'
     })
      .otherwise({
        redirectTo: '/'
      })

  }
  )
