
(function () {
  'use strict'

  angular
    .module('app')
    .controller('QAController', Controller)

  function Controller (UserFactory, $location, $routeParams, $rootScope) {
    var _this = this
    _this.errors = []
    $rootScope.errors = null
    $rootScope.messages = null

    activate()

    function activate () {
      UserFactory.getSessionUser(function (data) {
        if (data != null) {
          _this.user = data
        } else {
            $rootScope.errors = {message: 'you are not logged in'}
          $location.url('/')
        }
      })
    }
    _this.login = function () {
      _this.errors = []
      UserFactory.login(_this.user, function (data) {
        if (data.status) {
          _this.user = data.user
          $location.path('/questions')
        } else {
          _this.errors = data.errors
          $rootScope.errors = data.errors
        }
        _this.user = {}
      })
    }
    _this.logout = function () {
      UserFactory.logout(function (data) {
        _this.user = {}
        $location.url('/')
      })
    }

    _this.getQuestions = function () {
      UserFactory.getQuestions(function (data) {
        _this.questions = data.questions
      })
    }

    _this.getQuestion = function(id) {
       UserFactory.getQuestion(id, function(data){
         _this.questions = null
         _this.question = data.question
       })
    }

    //_this.like = function(question_id, answer_id) {
    _this.like = function(answer) {
       UserFactory.like(answer._id, function(data){
         if(data.status){
          answer.like += 1
         }else {
          _this.errors = data.errors
          $rootScope.errors = data.errors
         }
       })
    }

    _this.newQuestion = function() {
       $location.url('/newQuestion')
    }

    _this.newAnswer = function() {
       _this.question_id = $routeParams.id
    }

    _this.index = function() {
       _this.question = {}
       _this.answer = {}
       $location.url('/questions')
    }

    _this.createQuestion = function() {
       UserFactory.createQuestion(_this.question, function(data){
          if(data.status){
             $location.url('/questions')
          }else{
            _this.errors = data.errors
            $rootScope.errors = data.errors
          }
       })
    }

    _this.createAnswer = function() {
      UserFactory.createAnswer($routeParams.id, _this.answer, function(data){
        if(data.status){
          $location.url('/questions')
        } else{
          _this.errors = data.errors
          $rootScope.errors = data.errors
        }
      })
    }
  }

})()
