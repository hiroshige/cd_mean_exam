console.log('read routes')
const users = require('./../controllers/users.js')
const answers = require('./../controllers/answers.js')
const questions = require('./../controllers/questions.js')

module.exports = (app) => {
  app.post('/login', users.login)
  app.get('/logout', users.logout)
  app.get('/session', users.session)
  app.get('/questions', questions.index)
  app.get('/questions/:id', questions.show)
  app.post('/questions', questions.create)
  app.post('/answers/:id', answers.create)
  app.get('/answers/:id/like', answers.like)
}
