
console.log('question model')

const mongoose = require('mongoose')
const Schema = mongoose.Schema


const QuestionSchema = new Schema({
  question: {
    type: String,
    required: true,
    minlength: 10,
  },
  description: {
    type: String,
  },
  answers: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Answer'
    }

  ],
  _user:{
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }


},{ timestamps: {createdAt: 'created_at', updatedAt:'updated_at'} })


// register
//
mongoose.model('Question', QuestionSchema)
