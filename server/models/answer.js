console.log('anwer model')

const mongoose = require('mongoose')
const Schema = mongoose.Schema


const AnswerSchema = new Schema({
  answer: {
    type: String,
    required: true,
    minlength: 5,
  },
  detail: {
    type: String,
  },
  username: {
    type: String
  },
  like: {
    type: Number,
    default:0,
  },
  _question:{
    type: Schema.Types.ObjectId,
    ref: 'Question',
    required: true
  },
  _user:{
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }


},{ timestamps: {createdAt: 'created_at', updatedAt:'updated_at'} })

// register
mongoose.model('Answer', AnswerSchema)
