console.log('user model')

const mongoose = require('mongoose')
const Schema = mongoose.Schema


const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  questions: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Question'
    }

  ],
  answers:[
    {
      type: Schema.Types.ObjectId,
      ref: 'Answer'
    }

  ],

},{ timestamps: {createdAt: 'created_at', updatedAt:'updated_at'} })

//register
mongoose.model('User', UserSchema)
