


const mongoose = require('mongoose')
const Question = mongoose.model('Question')
const User = mongoose.model('User')

module.exports = {
  index: (req, res) => {
    if(!req.session['user_info']){
      res.json({status: false, errors: ['not login'], login_required:true})
    }else{

      let query = Question.find({})
      query.sort({created_at: -1})
      query.populate('answers')
      query.populate('_user')

      query.exec( (err, questions) => {
        if(err){
          res.json({status: false, errors:err})
        } else{
          res.json({status: true, questions: questions})
        }
      } )
    }
  }, //end index
  create: (req, res) => {
    if(!req.session['user_info']){
      res.json({status: false, errors: ['not login'], login_required:true})
    }else{
      let question = new Question(req.body)
      let user_id = req.session['user_info'].id
      User.findOne({_id: user_id}, (err, user) => {
        if(err){
          res.json({status:false, errors: err})
        }else{
          question._user = user
          question.save((err) => {
            if(err){
              res.json({status: false, errors: err})
            }else{
            user.questions.push(question)
            user.save((err)=>{
              if(err){
                res.json({status:false, errors:err})
              }
              res.json({status: true, question: question})
            }
            )}

            }
        )}

      })
    }
  }, //end create
  show: (req, res) => {
     let query = Question.findOne({_id: req.params.id})

    query.populate('answers')
    query.populate('_user')
     query.exec((err, question) => {
       if(err){
         res.json({status:false, errors:err})
       }else {
          res.json({status:true, question:question})
       }
     })
  }

}
