


const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId
const Question = mongoose.model('Question')
const User = mongoose.model('User')
const Answer = mongoose.model('Answer')

module.exports = {
  tmp: (req, res) => {
    res.json({status:true})
  },



  create: (req, res) => {
    if(!req.session['user_info']){
      res.json({status: false, errors: ['not login'], login_required:true})
    } else {
      let user_id = req.session['user_info'].id
      let question_id = req.params.id
      let answer = new Answer(req.body)
      let user
      let question
      answer._user = ObjectId(user_id)
      answer._question = ObjectId(question_id)
      // TODO: check existence of user_id and question_id
      answer.save()
        .then(()=>{
          return User.findOne({_id: user_id}).exec()
        })
        .then( (_user) => {
          user = _user
          _user.answers.push(answer)
          return _user.save()
        } )
        .then( () => {
          return Question.findOne({_id: question_id}).exec()
        } )
        .then( (_question) => {
          question = _question
          _question.answers.push(answer)
          return _question.save()
        } )
        .then(() =>{
          res.json({status: true, answer: answer})
        })
        .catch( (err) => {
           res.json({status:false, errors: err})
        } )
    }
  }, //end create

  like : (req, res) => {
    Answer.findOne({_id: req.params.id}).exec((err, answer) =>{
      if(err){
        res.json({status:false, errors:err})
      } else{
        answer.like += 1
        answer.save((err) =>{
          if(err){
            res.json({status:false, errors:err})
          }else{
            res.json({status:true, answer: answer})
          }
        })
      }

    })
  },

}
